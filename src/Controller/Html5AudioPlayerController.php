<?php


namespace Drupal\html5_audio_player\Controller;

/**
 * @file
 * Provides html5_audio_player functionality.
 */

use Drupal\Core\Controller\ControllerBase;
use Drupal\Core\Entity\Query\Sql\Query;
use Drupal\node\Entity\Node;
use Drupal\file\Entity\File;
use Drupal\media\Entity\Media;
use Drupal\image\Entity\ImageStyle;
use Drupal\media_entity\MediaInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Drupal\Core\Url;
use Drupal\Core\Database\Database;

class Html5AudioPlayerController extends ControllerBase{

  /**
   * {@inheritdoc}
   */
  public function htmlaudioplayer(){
    global $base_url;
    $output.= '<a href="'.$base_url.'/admin/config/development/add/html5_audio_player" id="html_add_player_button" class="button">Add New Player</a><a href="'.$base_url.'/admin/config/development/html5_audio_player/developer_info" class="button" id="html_developer_info_button">Developer Info</a><a href="https://dev-bpluginsdemo.pantheonsite.io/html5-audio-player-pro/" target="_blank" class="button" id="html_developer_info_button">Buy Pro Version</a>';
    $output.= '<br><br>';
    $output.= '<table class="responsive-enabled" data-striping="1"><thead><tr><th>Title</th><th>Token</th><th>Action</th></tr>
    </thead><tbody>';
    $database = \Drupal::database();
    $query = $database->select('html5_audio_player', 'h')
            ->fields('h', ['title', 'token', 'script']);
    $result = $query->execute();
    foreach ($result as $record) {
      $title = $record->title;
      $token = $record->token;
      $script = $record->script;
      $output.= '<tr class="odd"><td><b>'.$title.'</b></td><td><div class="one_line_custom"><div id='.$token.'>['.$token.']</div><div class="copyButton" id="copyButtonId" data-id="@item.Type" data-clipboard-action="copy" data-clipboard-target="div#'.$token.'"><img width="15" alt="example-2" src="https://clipboardjs.com/assets/images/clippy.svg"></div></div></td><td><div class="dropbutton-wrapper dropbutton-multiple"><div class="one_line_custom"><div id="html_add_player_button_delete"><a href="'.$base_url.'/admin/config/development/delete/html5_audio_player?token='.$token.'">Trash</a></div><div id="html_add_player_button_edit"><a href="'.$base_url.'/admin/config/development/edit/html5_audio_player/'.$token.'">Update Settings</a></div></div></div></td></tr>';
    }
    $output.='</tbody></table>';
    $build = ['#markup' => $output,];
    return $build;
  }

  /**
   * {@inheritdoc}
   */
  public function playertokendelete(){
    $get_url = \Drupal::request()->getRequestUri();
    $get_url_explode = explode('?', $get_url);
    $get_token = explode('=', $get_url_explode[1]);
    $get_token_value = trim($get_token[1]);
    if (!empty($get_token_value)) {
      $query = \Drupal::database()->delete('html5_audio_player')
              ->condition('token', $get_token_value)
              ->execute();
      $redirect = new RedirectResponse(Url::fromUserInput('/admin/config/development/html5_audio_player')->toString());
      $redirect->send();
      drupal_set_message(t('Player deleted successfully.'));
    }
    else {
      $redirect = new RedirectResponse(Url::fromUserInput('/admin/config/development/html5_audio_player')->toString());
      $redirect->send();
    }
  }

  /**
   * {@inheritdoc}
   */
  public function playerdeveloperinfo(){
    $output_developer_info.= '<div class="get-social-inner red-box"><h3 class="block-title-text">Developed By <a href="https://bplugins.com/">bPlugins</a></h3><h2 class="block-title-text-h2"><a href = "mailto: support@plugins.com">Get Support</a></h2><div class="block-title-text-h2">Need Customization or other Services ?</div><div class="block-title-text-h2"><a href = "mailto: hire@plugins.com">Get Quote</a></div><div class="block-title-text-h2">Contact us on Skype : ah_polash</div></div>';
    $build_developer_info = [
      '#markup' => $output_developer_info,
    ];
    return $build_developer_info;
  }
}