<?php

namespace Drupal\html5_audio_player\Form;

/**
 * @file
 * Provides html5_audio_player functionality.
 */

use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Database\Database;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Drupal\Core\Url;
use Drupal\Core\Entity\EntityStorageInterface;
use Drupal\file\Entity\File;
use Drupal\media\Entity\Media;

class Html5AudioPlayerEditForm extends FormBase
{
    
  /**
   * {@inheritdoc}
   */
  public function getFormId() {
      return 'html5_audio_player_edit_form';
  }
  
  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
  $current_path = \Drupal::service('path.current')->getPath();
  $current_path_explode = explode('/', $current_path);
  $get_token = $current_path_explode[6];
  $get_data_token = db_query("SELECT * FROM html5_audio_player WHERE token='".$get_token."'")->fetchAll();
  $get_title = $get_data_token[0]->title;
	$form['html5_audio_player_heading'] = array(
	    '#type' => 'markup',
	    '#markup' => '<h2>Update '.$get_title.' Player Settings</h2>',
	 );
  $form['html5_audio_player_fieldset_configue_setting'] = array(
      '#type' => 'fieldset',
      '#title' => t('Configure your Audio player'),
  );
  $form['html5_audio_player_fieldset_configue_setting']['html5_audio_player_token'] = array(
    '#type' => 'hidden',
    '#value' => $get_data_token[0]->token,
  );
  $form['html5_audio_player_fieldset_configue_setting']['html5_audio_player_upload_audio'] = array(
    '#type' => 'hidden',
    '#value' => $get_data_token[0]->audio_url,
  );
  if ($get_data_token[0]->repeat == 'loop') {
    $repeat_value = array('loop');
  }
  $form['html5_audio_player_fieldset_configue_setting']['html5_audio_player_repeat'] = array(
    '#type' => 'checkboxes',
    '#title' => t('Repeat'),
    '#default_value' => $repeat_value,
    '#options' => array('loop' => 'Loop'),
  );
  $form['html5_audio_player_fieldset_configue_setting']['html5_audio_player_muted_heading']= array(
      '#type' => 'markup',
      '#markup' => '<b>Muted</b>',
   );
  $form['html5_audio_player_fieldset_configue_setting']['html5_audio_player_muted'] = array(
    '#type' => 'checkbox',
    '#default_value' => $get_data_token[0]->muted,
    '#title' => $this->t('Check if you want the audio output should be muted.'),
  );
  if ($get_data_token[0]->autoplay == 'autoplay') {
    $autoplay_value = array('autoplay');
  }
  $form['html5_audio_player_fieldset_configue_setting']['html5_audio_player_autoplay'] = array(
    '#type' => 'checkboxes',
    '#title' => t('Auto Play'),
    '#default_value' => $autoplay_value,
    '#options' => array('autoplay' => 'Check if you want the audio will start playing as soon as it is ready.'),
  );
  $play_width_array = array(100 => 'None', 150 => '150px',200 => '200px', 250 => '250px', 300 => '300px', 350 => '350px', 400 => '400px', 450 => '450px', 500 => '500px', 550 => '550px', 600 => '600px', 650 => '650px', 700 => '700px', 750 => '750px', 800 => '800px', 850 => '850px', 900 => '900px', 950 => '950px', 1000 => '1000px', 1050 => '1050px', 1100 => '1100px', 1150 => '1150px', 1200 => '1200px', 1250 => '1250px', 1300 => '1300px', 1350 => '1350px', 1400 => '1400px');
  $form['html5_audio_player_fieldset_configue_setting']['html5_audio_player_player_width'] = array(
      '#type' => 'select',
      '#title' => t('Player Width'),
      '#default_value' => $get_data_token[0]->player_width,
      '#options' => $play_width_array,
  );
  $form['actions']['#type'] = 'actions';
  $form['submit'] = array(
    '#type' => 'submit',
    '#value' => t('Update Player Settings'),
  );
  return $form;
}
    
  /**
 * {@inheritdoc}
 */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $audio_url_type = $form_state->getValue('html5_audio_player_upload_audio');
    $audio_explode = explode(',', $audio_url_type);
    $url = $audio_explode[0];
    $file_type = $audio_explode[1];
    $repeat_element = $form_state->getValue('html5_audio_player_repeat');
    $muted_element = $form_state->getValue('html5_audio_player_muted');
    $autoplay_element = $form_state->getValue('html5_audio_player_autoplay');
    $player_token_generate = $form_state->getValue('html5_audio_player_token');
    $player_width_element = $form_state->getValue('html5_audio_player_player_width');
    $player_width_element_for_edit = $form_state->getValue('html5_audio_player_player_width');
    if (empty($player_width_element)) {
      $player_width_element = '';
    }
    else {
      $player_width_element = 'style="width: '.$player_width_element.'px;"';
    }
    $create_script.= '<script src="https://cdn.plyr.io/3.5.10/plyr.polyfilled.js"></script><link rel="stylesheet" href="https://cdn.plyr.io/3.5.10/plyr.css" /><audio id="player_'.$player_token_generate.'" class="'.$player_token_generate.'" controls '.$player_width_element.'><source src="'.$url.'" type="'.$file_type.'" /></audio>';
    $create_script.= '<script>';
    $create_script.= 'const player_'.$player_token_generate.' = new Plyr("#player_'.$player_token_generate.'",{debug: true';
    if (empty($repeat_element) || $repeat_element['loop'] == '0') {
      $create_script.= '';
    }
    else {
      $create_script.= ',loop:{active:true}';
    }
    if ($muted_element == 0) {
      $create_script.= '';
    }
    else {
      $create_script.= ',muted: true';
    }
    if (empty($autoplay_element) || $autoplay_element['autoplay'] == '0') {
      $create_script.= '';
    }
    else {
      $create_script.= ',autoplay: true';
    }    
    $create_script.= ", controls : [";
    $create_script.= ",'play-large', 'play'";
    $create_script.= ", 'progress', 'mute', 'volume', 'download','captions', 'pip', 'airplay', 'fullscreen'";    
    $create_script.= "]});</script>";
    $conn = Database::getConnection();
    $conn->update('html5_audio_player')->fields(
      array(
        'script' => $create_script,
        'repeat' => $repeat_element['loop'],
        'muted' => $muted_element,
        'autoplay' => $autoplay_element['autoplay'],
        'player_width' => $player_width_element_for_edit,
      )
    )
    ->condition('token', $player_token_generate, '=')
    ->execute();
    $redirect = new RedirectResponse(Url::fromUserInput('/admin/config/development/html5_audio_player')->toString());
    $redirect->send();
    drupal_set_message(t('Player Updated successfully .'));
  }
}
