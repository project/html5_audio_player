<?php

namespace Drupal\html5_audio_player\Form;

/**
 * @file
 * Provides html5_audio_player functionality.
 */

use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Database\Database;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Drupal\Core\Url;
use Drupal\Core\Entity\EntityStorageInterface;
use Drupal\file\Entity\File;
use Drupal\media\Entity\Media;

class Html5AudioPlayerForm extends FormBase
{
    
  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'html5_audio_player_form';
  }
  
  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {

  	$form['html5_audio_player_heading'] = array(
	    '#type' => 'markup',
	    '#markup' => '<h2>Add New Player</h2>',
  	);        
    $form['html5_audio_player_title'] = array(
      '#title' => t('Player Title'),
      '#type' => 'textfield',
      '#attributes' => array('placeholder' => t('Add title')),
      '#required' => TRUE,
    );        
    $form['html5_audio_player_fieldset_configue_setting'] = array(
      '#type' => 'fieldset',
      '#title' => t('Configure your Audio player'),
    );
    $validators = array(
      'file_validate_extensions' => array('wav mp3 ogg'),
    );
    $form['html5_audio_player_fieldset_configue_setting']['html5_audio_player_upload_audio'] = array(
      '#type' => 'managed_file',
      '#title' => 'Upload Audio',
      '#description' => $this->t('Supported file format (.wav .mp3 .ogg file)').': '.\Drupal::state()->get('html5_audio_player_upload_audio_NAME'),
      '#upload_location' => 'public://',
      '#upload_validators' => $validators,
    );
    $form['html5_audio_player_fieldset_configue_setting']['html5_audio_player_repeat'] = array(
      '#type' => 'checkboxes',
      '#title' => t('Repeat'),
      '#options' => array('loop' => 'Loop'),
    );
    $form['html5_audio_player_fieldset_configue_setting']['html5_audio_player_muted_heading']= array(
      '#type' => 'markup',
      '#markup' => '<b>Muted</b>',
     );
    $form['html5_audio_player_fieldset_configue_setting']['html5_audio_player_muted'] = array(
      '#type' => 'checkbox',
      '#title' => $this->t('Check if you want the audio output should be muted.'),
    );
    $form['html5_audio_player_fieldset_configue_setting']['html5_audio_player_autoplay'] = array(
      '#type' => 'checkboxes',
      '#title' => t('Auto Play'),
      '#options' => array('autoplay' => 'Check if you want the audio will start playing as soon as it is ready.'),
    );
    $play_width_array = array(100 => 'None', 150 => '150px',200 => '200px', 250 => '250px', 300 => '300px', 350 => '350px', 400 => '400px', 450 => '450px', 500 => '500px', 550 => '550px', 600 => '600px', 650 => '650px', 700 => '700px', 750 => '750px', 800 => '800px', 850 => '850px', 900 => '900px', 950 => '950px', 1000 => '1000px', 1050 => '1050px', 1100 => '1100px', 1150 => '1150px', 1200 => '1200px', 1250 => '1250px', 1300 => '1300px', 1350 => '1350px', 1400 => '1400px');
    $form['html5_audio_player_fieldset_configue_setting']['html5_audio_player_player_width'] = array(
      '#type' => 'select',
      '#title' => t('Player Width'),
      '#options' => $play_width_array,
    );
    $form['actions']['#type'] = 'actions';
    $form['submit'] = array(
        '#type' => 'submit',
        '#value' => t('Save Player Settings'),
    );
    return $form;
}

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $validators = array('file_validate_extensions' => array('wav mp3 ogg'));
    $files = file_save_upload('html5_audio_player_upload_audio', $validators, 'public://', FILE_EXISTS_REPLACE);
    $file = File::load($files[0]);
    if($file) {
      kint($files);
      exit;
      $file->setPermanent();
      $file->save();
    }
    $id = db_query("SELECT MAX(fid) as max_id FROM file_managed")->fetchCol();
    $file = \Drupal\file\Entity\File::load($id[0]);
    $uri = $file->getFileUri();
    $url = \Drupal\Core\Url::fromUri(file_create_url($uri))->toString();
    $file_type = $file->getMimeType();
    $repeat_element = $form_state->getValue('html5_audio_player_repeat');
    $muted_element = $form_state->getValue('html5_audio_player_muted');
    $autoplay_element = $form_state->getValue('html5_audio_player_autoplay');
    $player_title = $form_state->getValue('html5_audio_player_title');
    $player_token_replace = str_replace(" ","_",$player_title);
    $player_token_generate = $player_token_replace.'_token';
    $player_width_element = $form_state->getValue('html5_audio_player_player_width');
    $player_width_element_for_edit = $form_state->getValue('html5_audio_player_player_width');
    if (empty($player_width_element)) {
      $player_width_element = '';
    }
    else {
      $player_width_element = 'style="width: '.$player_width_element.'px;"';
    }
    $create_script.= '<script src="https://cdn.plyr.io/3.5.10/plyr.polyfilled.js"></script><link rel="stylesheet" href="https://cdn.plyr.io/3.5.10/plyr.css" /><audio id="player_'.$player_token_generate.'" class="'.$player_token_generate.'" controls '.$player_width_element.'><source src="'.$url.'" type="'.$file_type.'" /></audio>';
    $create_script.= '<script>';
    $create_script.= 'const player_'.$player_token_generate.' = new Plyr("#player_'.$player_token_generate.'",{debug: true';
    if (empty($repeat_element) || $repeat_element['loop'] == '0') {
      $create_script.= '';
    }
    else {
      $create_script.= ',loop:{active:true}';
    }
    if ($muted_element == 1) {
      $create_script.= ',muted: true';
    }
    else {
      $create_script.= '';
    }
    if (empty($autoplay_element) || $autoplay_element['autoplay'] == '0') {
      $create_script.= '';
    }
    else {
      $create_script.= ',autoplay: true';
    }
    $create_script.= ", controls : [";
    $create_script.= ",'play-large', 'play'";
    $create_script.= ", 'progress', 'mute', 'volume', 'download','captions', 'pip', 'airplay', 'fullscreen'";
    $create_script.= "]});</script>";
    $conn = Database::getConnection();
    $conn->insert('html5_audio_player')->fields(
      array(
        'title' => $player_title,
        'token' => $player_token_generate,
        'script' => $create_script,
        'audio_url' => $url.','.$file_type,
        'repeat' => $repeat_element['loop'],
        'muted' => $muted_element,
        'autoplay' => $autoplay_element['autoplay'],
        'player_width' => $player_width_element_for_edit,    
      )
    )->execute();
    $redirect = new RedirectResponse(Url::fromUserInput('/admin/config/development/html5_audio_player')->toString());
    $redirect->send();
    drupal_set_message(t('Player added successfully .'));
  }
}